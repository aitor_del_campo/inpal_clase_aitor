﻿using System;
using System.IO;
using SQLite.Net;
using SQLite.Net.Async;
using Xamarin.Forms;

[assembly: Dependency(typeof(INPAL.SQLiteIos))]

namespace INPAL
{
    public class SQLiteIos : ISQLite
    {
        public SQLiteIos()
        {
        }

        string DbPath
        {
            get
            {
                var sqliteFilename = "INPALSQLite.db3";
                string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                return Path.Combine(documentsPath, sqliteFilename);
            }
        }

        public SQLiteAsyncConnection GetAsyncConnection()
        {
            var sqliteConnection = new SQLiteConnectionWithLock(new SQLite.Net.Platform.XamarinIOS.SQLitePlatformIOS(), new SQLiteConnectionString(DbPath, true));
            var connFactory = new Func<SQLiteConnectionWithLock>(() => sqliteConnection);
            var conn = new SQLiteAsyncConnection(connFactory);
            return conn;
        }

        public SQLiteConnection GetConnection()
        {
            var conn = new SQLiteConnection(new SQLite.Net.Platform.XamarinIOS.SQLitePlatformIOS(), DbPath, true);
            return conn;
        }

    }
}
