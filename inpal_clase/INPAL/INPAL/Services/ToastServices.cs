﻿using System;
using System.Collections.Generic;
using System.Text;

using Plugin.Toasts;

using System.Globalization;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace INPAL
{
    public class ToastService
    {

        private CultureInfo ci;

        #region Properties

        static ToastService _instance;
        public static ToastService Instance
        {
            get
            {
                return _instance ?? (_instance = new ToastService());
            }
        }

        #endregion

        public void Init()
        {

            if (Device.RuntimePlatform == Device.iOS || Device.RuntimePlatform == Device.Android)
            {
                ci = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
            }

            MessagingCenter.Unsubscribe<SearchViewModel>(this, Constants.notif_conexion);
            MessagingCenter.Subscribe<SearchViewModel>(this, Constants.notif_conexion, async (sender) =>
            {
                await ShowToast(ToastNotificationType.Warning, "No tiene conexión a internet", "A continuación se mostrarán los datos almacenados en su teléfono");
            });

        }

        public async Task ShowToast(ToastNotificationType type, string title, string text)
        {
            var notificator = DependencyService.Get<IToastNotificator>();
            await notificator.Notify(type, title, text, TimeSpan.FromSeconds(3));
        }
    }
}