﻿using FireSharp;
using FireSharp.Config;
using FireSharp.Interfaces;
using FireSharp.Response;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace INPAL
{
    public class RestServices : IRestServices
    {
        //Declaracion del cliente firebase para nuget FireSharp
        private static FirebaseClient _client;

        //Declaracion
        IFirebaseConfig config = new FirebaseConfig
        {
            BasePath = Constants.BasePath
        };

        #region Properties

        static RestServices _instance;

        public static RestServices Instance
        {
            get
            {
                return _instance ?? (_instance = new RestServices());
            }
        }

        private void SetConnection()
        {
            client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
        }

        HttpClient client;

        string wsUrl;

        #endregion

        #region Functions

        public async Task RunSafe(Func<Task> execute, bool notifyOnError = true)
        {
            Exception exception = null;

            try
            {
                await execute();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"ERROR {0}", ex.Message);
            }

            if (exception != null)
            {
                Debug.WriteLine(exception);

                if (notifyOnError)
                {
                    MessagingCenter.Send<RestServices, Exception>(this, "ExceptionOccurred", exception);
                }
            }
        }

        public async Task<HttpResponseMessage> CheckAndExecuteGetRequestAsync(Uri uri)
        {

            HttpResponseMessage response = await RunSafe(async () =>
            {
                return await client.GetAsync(uri);
            });

            try
            {
                if (response.IsSuccessStatusCode)
                {
                    return response;
                }
                else
                {
                    Debug.WriteLine("Error retrieving info from server");
                    return null;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"ERROR {0}", ex.Message);
                return null;
            }
        }

       
        public async Task<List<INPAL>> GetINPALListAsync()
        {
            List<INPAL> result = new List<INPAL>();


            _client = new FirebaseClient(config);

            FirebaseResponse response = await _client.GetAsync("INPAL");
            result = response.ResultAs<List<INPAL>>();
            return result;
        }

        public async Task<List<Fecha>> GetFechaAsync()
        {
            List<Fecha> result = new List<Fecha>();

            _client = new FirebaseClient(config);

            FirebaseResponse response = await _client.GetAsync("Fecha");
            result = response.ResultAs<List<Fecha>>();
            return result;
        }

        #endregion

        #region Helpers
        public async Task<HttpResponseMessage> RunSafe(Func<Task<HttpResponseMessage>> execute)
        {
            try
            {
                SetConnection();

                var response = await execute();
                return response;
            }
            catch (TaskCanceledException ex)
            {
                throw ex;
            }
            catch (OperationCanceledException ex)
            {
                throw ex;
            }
            catch (HttpRequestException ex)
            {
                throw ex;
            }
            catch (Newtonsoft.Json.JsonException)
            {
                // We catch JsonException and throw it as a regular HttpException because
                // usually it is due to some proxy or gateway in the middle retrieving an error
                throw new HttpRequestException();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

 

        #endregion

    }
}