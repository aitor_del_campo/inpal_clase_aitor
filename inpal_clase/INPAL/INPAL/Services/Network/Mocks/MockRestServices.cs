﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace INPAL
{
    /// <summary>
    /// Mocks related to server methods
    /// </summary>
    public class MockRestServices : IRestServices
    {
        #region Functions

        public async Task<List<INPAL>> GetINPALListAsync()
        {
            List<INPAL> result = null;

            var jsonText = GetContentJsonFile("INPAL.Services.Network.Mocks.Json.INPAL.json");
            if (!string.IsNullOrEmpty(jsonText))
            {
                result = JsonConvert.DeserializeObject<List<INPAL>>(jsonText);
            }

            return result;
        }

        public async Task<List<Fecha>> GetFechaAsync()
        {
            List<Fecha> result = null;

            var jsonText = GetContentJsonFile("INPAL.Services.Network.Mocks.Json.Fecha.json");
            if (!string.IsNullOrEmpty(jsonText))
            {
                result = JsonConvert.DeserializeObject<List<Fecha>>(jsonText);
            }

            return result;
        }


        string GetContentJsonFile(string filename)
        {
            string text = string.Empty;
            var assembly = typeof(App).GetTypeInfo().Assembly;
            Stream stream = assembly.GetManifestResourceStream(filename);
            using (var reader = new System.IO.StreamReader(stream))
            {
                text = reader.ReadToEnd();
            }
            return text;
        }
        #endregion
    }

}