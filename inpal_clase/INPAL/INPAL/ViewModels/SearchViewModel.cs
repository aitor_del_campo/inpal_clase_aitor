﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using System.Threading.Tasks;
using System.Linq;
using System.ComponentModel;

namespace INPAL
{
    public class SearchViewModel : BaseViewModel
    {
        #region Constructors

        public SearchViewModel()
        {
            InitialiceDataAsync();           
        }

        #endregion

        #region Properties
        private List<INPAL> _iNPALList;
        public List<INPAL> INPALList
        {
            get
            {
                return _iNPALList;
            }
            set
            {
                _iNPALList = value;
                NotifyPropertyChanged();
            }
        }

        private List<INPAL> _InpalListDB;
        public List<INPAL> InpalListDB
        {
            get
            {
                return _InpalListDB;
            }
            set
            {
                _InpalListDB = value;
                NotifyPropertyChanged();
            }
        }

        private string _searchText;
        public string SearchText
        {
            get
            {
                return _searchText;
            }
            set
            {
                _searchText = value;
                NotifyPropertyChanged();
            }
        }        
        #endregion

        #region filtros        

        private bool _isAllOn = false;
        public bool IsAllOn
        {
            get
            {
                return _isAllOn;
            }
            set
            {
                _isAllOn = value;
                NotifyPropertyChanged();
            }
        }

        private bool _isAllTextOn = false;
        public bool IsAllTextOn
        {
            get
            {
                return _isAllTextOn;
            }
            set
            {
                _isAllTextOn = value;
                NotifyPropertyChanged();
            }
        }

        private bool _isAllTextColorOn = false;
        public bool IsAllTextColorOn
        {
            get
            {
                return _isAllTextColorOn;
            }
            set
            {
                _isAllTextColorOn = value;
                NotifyPropertyChanged();
            }
        }        

        private bool _isPoligonilloOn = true;
        public bool IsPoligonilloOn
        {
            get
            {
                return _isPoligonilloOn;
            }
            set
            {
                _isPoligonilloOn = value;
                NotifyPropertyChanged();
            }
        }

        private bool _isAntolinOn = true;
        public bool IsAntolinOn
        {
            get
            {
                return _isAntolinOn;
            }
            set
            {
                _isAntolinOn = value;
                NotifyPropertyChanged();
            }
        }

        private bool _isVillalobonOn = true;
        public bool IsVillalobonOn
        {
            get
            {
                return _isVillalobonOn;
            }
            set
            {
                _isVillalobonOn = value;
                NotifyPropertyChanged();
            }
        }

        private bool _isAngelesOn = true;
        public bool IsAngelesOn
        {
            get
            {
                return _isAngelesOn;
            }
            set
            {
                _isAngelesOn = value;
                NotifyPropertyChanged();
            }
        }

        private bool _isAgrariaOn = true;
        public bool IsAgrariaOn
        {
            get
            {
                return _isAgrariaOn;
            }
            set
            {
                _isAgrariaOn = value;
                NotifyPropertyChanged();
            }
        }
        private bool _isDeportivaOn = true;
        public bool IsDeportivaOn
        {
            get
            {
                return _isDeportivaOn;
            }
            set
            {
                _isDeportivaOn = value;
                NotifyPropertyChanged();
            }
        }
        private bool _isGestionOn = true;
        public bool IsGestionOn
        {
            get
            {
                return _isGestionOn;
            }
            set
            {
                _isGestionOn = value;
                NotifyPropertyChanged();
            }
        }
        private bool _isGraficasOn = true;
        public bool IsGraficasOn
        {
            get
            {
                return _isGraficasOn;
            }
            set
            {
                _isGraficasOn = value;
                NotifyPropertyChanged();
            }
        }
        private bool _isArtesOn = true;
        public bool IsArtesOn
        {
            get
            {
                return _isArtesOn;
            }
            set
            {
                _isArtesOn = value;
                NotifyPropertyChanged();
            }
        }
        private bool _isComercioOn = true;
        public bool IsComercioOn
        {
            get
            {
                return _isComercioOn;
            }
            set
            {
                _isComercioOn = value;
                NotifyPropertyChanged();
            }
        }
        private bool _isElectricidadOn = true;
        public bool IsElectricidadOn
        {
            get
            {
                return _isElectricidadOn;
            }
            set
            {
                _isElectricidadOn = value;
                NotifyPropertyChanged();
            }
        }
        private bool _isEnergiaOn = true;
        public bool IsEnergiaOn
        {
            get
            {
                return _isEnergiaOn;
            }
            set
            {
                _isEnergiaOn = value;
                NotifyPropertyChanged();
            }
        }
        private bool _isHosteleriaOn = true;
        public bool IsHosteleriaOn
        {
            get
            {
                return _isHosteleriaOn;
            }
            set
            {
                _isHosteleriaOn = value;
                NotifyPropertyChanged();
            }
        }
        private bool _isPersonalOn = true;
        public bool IsPersonalOn
        {
            get
            {
                return _isPersonalOn;
            }
            set
            {
                _isPersonalOn = value;
                NotifyPropertyChanged();
            }
        }
        private bool _isImagenOn = true;
        public bool IsImagenOn
        {
            get
            {
                return _isImagenOn;
            }
            set
            {
                _isImagenOn = value;
                NotifyPropertyChanged();
            }
        }
        private bool _isAlimentariasOn = true;
        public bool IsAlimentariasOn
        {
            get
            {
                return _isAlimentariasOn;
            }
            set
            {
                _isAlimentariasOn = value;
                NotifyPropertyChanged();
            }
        }
        private bool _isExtractivasOn = true;
        public bool IsExtractivasOn
        {
            get
            {
                return _isExtractivasOn;
            }
            set
            {
                _isExtractivasOn = value;
                NotifyPropertyChanged();
            }
        }
        private bool _isInformaticaOn = true;
        public bool IsInformaticaOn
        {
            get
            {
                return _isInformaticaOn;
            }
            set
            {
                _isInformaticaOn = value;
                NotifyPropertyChanged();
            }
        }
        private bool _isMantenimientoOn = true;
        public bool IsMantenimientoOn
        {
            get
            {
                return _isMantenimientoOn;
            }
            set
            {
                _isMantenimientoOn = value;
                NotifyPropertyChanged();
            }
        }
        private bool _isMaderaOn = true;
        public bool IsMaderaOn
        {
            get
            {
                return _isMaderaOn;
            }
            set
            {
                _isMaderaOn = value;
                NotifyPropertyChanged();
            }
        }
        private bool _isPesqueraOn = true;
        public bool IsPesqueraOn
        {
            get
            {
                return _isPesqueraOn;
            }
            set
            {
                _isPesqueraOn = value;
                NotifyPropertyChanged();
            }
        }
        private bool _isQuímicaOn = true;
        public bool IsQuímicaOn
        {
            get
            {
                return _isQuímicaOn;
            }
            set
            {
                _isQuímicaOn = value;
                NotifyPropertyChanged();
            }
        }
        private bool _isSanidadOn = true;
        public bool IsSanidadOn
        {
            get
            {
                return _isSanidadOn;
            }
            set
            {
                _isSanidadOn = value;
                NotifyPropertyChanged();
            }
        }
        private bool _isSeguridadOn = true;
        public bool IsSeguridadOn
        {
            get
            {
                return _isSeguridadOn;
            }
            set
            {
                _isSeguridadOn = value;
                NotifyPropertyChanged();
            }
        }
        private bool _isSocioculturalesOn = true;
        public bool IsSocioculturalesOn
        {
            get
            {
                return _isSocioculturalesOn;
            }
            set
            {
                _isSocioculturalesOn = value;
                NotifyPropertyChanged();
            }
        }
        private bool _isTextilOn = true;
        public bool IsTextilOn
        {
            get
            {
                return _isTextilOn;
            }
            set
            {
                _isTextilOn = value;
                NotifyPropertyChanged();
            }
        }
        private bool _isTransporteOn = true;
        public bool IsTransporteOn
        {
            get
            {
                return _isTransporteOn;
            }
            set
            {
                _isTransporteOn = value;
                NotifyPropertyChanged();
            }
        }
        private bool _isVidrioOn = true;
        public bool IsVidrioOn
        {
            get
            {
                return _isVidrioOn;
            }
            set
            {
                _isVidrioOn = value;
                NotifyPropertyChanged();
            }
        }

        private bool _isEdificacionOn = true;
        public bool IsEdificacionOn
        {
            get
            {
                return _isEdificacionOn;
            }
            set
            {
                _isEdificacionOn = value;
                NotifyPropertyChanged();
            }
        }

        private bool _isFabricacionOn = true;
        public bool IsFabricacionOn
        {
            get
            {
                return _isFabricacionOn;
            }
            set
            {
                _isFabricacionOn = value;
                NotifyPropertyChanged();
            }
        }
        #endregion

        #region Function     

        private async Task ShowINPALSQLiteAsync()
        {
            //await DataManager.Instance.DeleteSQLiteyAsync();
            using (new Busy(this))
            {
                if (Online)
                { 
                    List<INPAL> companies = await RestManager.Instance.GetINPALList();

                    foreach (INPAL company in companies)
                    {
                        INPAL activityDB = await DataManager.Instance.GetINPALByNameAsync(company.Name);
                        if (activityDB == null) { await DataManager.Instance.SaveINPALAsync(company); }
                        else { await DataManager.Instance.UpdateINPALAsync(company); }
                    }
                }

                else
                {
                    INPALList = await DataManager.Instance.GetINPALAsync();
                }

            }
        }

        private async Task InitialiceDataAsync()
        {
            using (new Busy(this))
            {

                InpalListDB = await DataManager.Instance.GetINPALAsync();
                INPALList = InpalListDB;

                if (!Online) { MessagingCenter.Send<SearchViewModel>(this, Constants.notif_conexion);}
            }
        }

        public async Task ExecuteSearchAsync(string text)
        {
            using (new Busy(this))
            {                
                IEnumerable<INPAL> resultado = InpalListDB.Where(x => x.Name.ToLower().Contains(text.ToLower()) || x.Description.ToLower().Contains(text.ToLower()));
                INPALList = new List<INPAL>(resultado);
            }
        }

        private async Task ExecuteFiltersAsync()
        {
            INPALList = await DataManager.Instance.GetINPALByPolygonSQLAsync(
                IsAntolinOn, IsPoligonilloOn, IsVillalobonOn, IsAngelesOn,
                IsAgrariaOn, IsEdificacionOn, IsFabricacionOn, IsDeportivaOn,
                IsGestionOn, IsGraficasOn, IsArtesOn, IsComercioOn, IsElectricidadOn,
                IsEnergiaOn, IsHosteleriaOn, IsPersonalOn, IsImagenOn, IsAlimentariasOn,
                IsExtractivasOn, IsInformaticaOn, IsMantenimientoOn, IsMaderaOn, IsPesqueraOn,
                IsQuímicaOn, IsSanidadOn, IsSeguridadOn, IsSocioculturalesOn, IsTextilOn,
                IsTransporteOn, IsVidrioOn);
        }

        public async Task PoligonilloInpalAsync()
        {          
            using (new Busy(this))
            {         
                IsPoligonilloOn = !IsPoligonilloOn;

                await ExecuteFiltersAsync();
            }
        }

        public async Task AntolinInpalAsync()
        {
            using (new Busy(this))
            {       
                IsAntolinOn = !IsAntolinOn;

                await ExecuteFiltersAsync();
            }
        }

        public async Task AngelesInpalAsync()
        {
            using (new Busy(this))
            {           
                IsAngelesOn = !IsAngelesOn;

                await ExecuteFiltersAsync();
            }
        }

        public async Task VillalobonInpalAsync()
        {
            using (new Busy(this))
            {               
                IsVillalobonOn = !IsVillalobonOn;

                await ExecuteFiltersAsync();
            }
        }

        public async Task AgrariaFilterAsync()
        {
            using (new Busy(this))
            {
                IsAgrariaOn = !IsAgrariaOn;              

                await ExecuteFiltersAsync();
            }
        }
        public async Task EdificacionFilterAsync()
        {
            using (new Busy(this))
            {
                IsEdificacionOn = !IsEdificacionOn;

                await ExecuteFiltersAsync();
            }
        }
        public async Task FabricacionFilterAsync()
        {
            using (new Busy(this))
            {
                IsFabricacionOn = !IsFabricacionOn;

                await ExecuteFiltersAsync();
            }
        }
        public async Task DeportivaFilterAsync()
        {
            using (new Busy(this))
            {
                IsDeportivaOn = !IsDeportivaOn;

                await ExecuteFiltersAsync();
            }
        }
        public async Task GestionFilterAsync()
        {
            using (new Busy(this))
            {
                IsGestionOn = !IsGestionOn;

                await ExecuteFiltersAsync();
            }
        }
        public async Task GraficasFilterAsync()
        {
            using (new Busy(this))
            {
                IsGraficasOn = !IsGraficasOn;

                await ExecuteFiltersAsync();
            }
        }
        public async Task ArtesFilterAsync()
        {
            using (new Busy(this))
            {
                IsArtesOn = !IsArtesOn;

                await ExecuteFiltersAsync();
            }
        }
        public async Task ComercioFilterAsync()
        {
            using (new Busy(this))
            {
                IsComercioOn = !IsComercioOn;

                await ExecuteFiltersAsync();
            }
        }
        public async Task ElectricidadFilterAsync()
        {
            using (new Busy(this))
            {
                IsElectricidadOn = !IsElectricidadOn;

                await ExecuteFiltersAsync();
            }
        }
        public async Task EnergiaFilterAsync()
        {
            using (new Busy(this))
            {
                IsEnergiaOn = !IsEnergiaOn;

                await ExecuteFiltersAsync();
            }
        }
        public async Task HosteleriaFilterAsync()
        {
            using (new Busy(this))
            {
                IsHosteleriaOn = !IsHosteleriaOn;

                await ExecuteFiltersAsync();
            }
        }
        public async Task PersonalFilterAsync()
        {
            using (new Busy(this))
            {
                IsPersonalOn = !IsPersonalOn;

                await ExecuteFiltersAsync();
            }
        }
        public async Task ImagenFilterAsync()
        {
            using (new Busy(this))
            {
                IsImagenOn = !IsImagenOn;

                await ExecuteFiltersAsync();
            }
        }
        public async Task AlimentariasFilterAsync()
        {
            using (new Busy(this))
            {
                IsAlimentariasOn = !IsAlimentariasOn;

                await ExecuteFiltersAsync();
            }
        }
        public async Task ExtractivasFilterAsync()
        {
            using (new Busy(this))
            {
                IsExtractivasOn = !IsExtractivasOn;

                await ExecuteFiltersAsync();
            }
        }
        public async Task InformaticaFilterAsync()
        {
            using (new Busy(this))
            {
                IsInformaticaOn = !IsInformaticaOn;

                await ExecuteFiltersAsync();
            }
        }
        public async Task MantenimientoFilterAsync()
        {
            using (new Busy(this))
            {
                IsMantenimientoOn = !IsMantenimientoOn;

                await ExecuteFiltersAsync();
            }
        }
        public async Task MaderaFilterAsync()
        {
            using (new Busy(this))
            {
                IsMaderaOn = !IsMaderaOn;

                await ExecuteFiltersAsync();
            }
        }
        public async Task PesqueraFilterAsync()
        {
            using (new Busy(this))
            {
                IsPesqueraOn = !IsPesqueraOn;

                await ExecuteFiltersAsync();
            }
        }
        public async Task QuímicaFilterAsync()
        {
            using (new Busy(this))
            {
                IsQuímicaOn = !IsQuímicaOn;

                await ExecuteFiltersAsync();
            }
        }
        public async Task SanidadFilterAsync()
        {
            using (new Busy(this))
            {
                IsSanidadOn = !IsSanidadOn;

                await ExecuteFiltersAsync();
            }
        }
        public async Task SeguridadFilterAsync()
        {
            using (new Busy(this))
            {
                IsSeguridadOn = !IsSeguridadOn;

                await ExecuteFiltersAsync();
            }
        }
        public async Task SocioculturalesFilterAsync()
        {
            using (new Busy(this))
            {
                IsSocioculturalesOn = !IsSocioculturalesOn;

                await ExecuteFiltersAsync();
            }
        }
        public async Task TextilFilterAsync()
        {
            using (new Busy(this))
            {
                IsTextilOn = !IsTextilOn;

                await ExecuteFiltersAsync();
            }
        }
        public async Task TransporteFilterAsync()
        {
            using (new Busy(this))
            {
                IsTransporteOn = !IsTransporteOn;

                await ExecuteFiltersAsync();
            }
        }
        public async Task VidrioFilterAsync()
        {
            using (new Busy(this))
            {
                IsVidrioOn = !IsVidrioOn;

                await ExecuteFiltersAsync();
            }
        }

        public async Task FilterButtonAsync()
        {
            using (new Busy(this))
            {
                if (IsAllOn == true)
                {
                    IsAgrariaOn = true;
                    IsEdificacionOn = true;
                    IsFabricacionOn = true;
                    IsDeportivaOn = true;
                    IsGestionOn = true;
                    IsGraficasOn = true;
                    IsArtesOn = true;
                    IsComercioOn = true;
                    IsElectricidadOn = true;
                    IsEnergiaOn = true;
                    IsHosteleriaOn = true;
                    IsPersonalOn = true;
                    IsImagenOn = true;
                    IsAlimentariasOn = true;
                    IsExtractivasOn = true;
                    IsInformaticaOn = true;
                    IsMantenimientoOn = true;
                    IsMaderaOn = true;
                    IsQuímicaOn = true;
                    IsPesqueraOn = true;
                    IsSanidadOn = true;
                    IsSeguridadOn = true;
                    IsSocioculturalesOn = true;
                    IsTextilOn = true;
                    IsTransporteOn = true;
                    IsVidrioOn = true;
                }

                else
                {
                    IsAgrariaOn = false;
                    IsEdificacionOn = false;
                    IsFabricacionOn = false;
                    IsDeportivaOn = false;
                    IsGestionOn = false;
                    IsGraficasOn = false;
                    IsArtesOn = false;
                    IsComercioOn = false;
                    IsElectricidadOn = false;
                    IsEnergiaOn = false;
                    IsHosteleriaOn = false;
                    IsPersonalOn = false;
                    IsImagenOn = false;
                    IsAlimentariasOn = false;
                    IsExtractivasOn = false;
                    IsInformaticaOn = false;
                    IsMantenimientoOn = false;
                    IsMaderaOn = false;
                    IsQuímicaOn = false;
                    IsPesqueraOn = false;
                    IsSanidadOn = false;
                    IsSeguridadOn = false;
                    IsSocioculturalesOn = false;
                    IsTextilOn = false;
                    IsTransporteOn = false;
                    IsVidrioOn = false;
                }

                IsAllTextOn = !IsAllTextOn;
                IsAllOn = !IsAllOn;
                IsAllTextColorOn = !IsAllTextColorOn;

                await ExecuteFiltersAsync();                
            }
        }
        /*private async Task ToListActivityAsync()
        {
            using (new Busy(this))
            {           
                ActivityList = await DataManager.Instance.GetActivitiesAsync();                
            }
        }*/

        #endregion

        #region Command
        Command _loadInfoCommand;
        public Command LoadInfoCommand

        {
            get
            {
                return _loadInfoCommand ?? (_loadInfoCommand = new Command(
                    async () => { await ShowINPALSQLiteAsync(); },
                    () => CanReload()
                ));
            }
        }        

        Command _poligonillofilterCommand;
        public Command poligonilloFilter
        {
            get
            {
                return _poligonillofilterCommand ?? (_poligonillofilterCommand = new Command(
                    async () => { await PoligonilloInpalAsync(); },
                    () => CanReload()
                ));
            }
        }

        Command _antolinfilterCommand;
        public Command antolinFilter
        {
            get
            {
                return _antolinfilterCommand ?? (_antolinfilterCommand = new Command(
                    async () => { await AntolinInpalAsync(); },
                    () => CanReload()
                ));
            }
        }

        Command _angelesfilterCommand;
        public Command angelesFilter
        {
            get
            {
                return _angelesfilterCommand ?? (_angelesfilterCommand = new Command(
                    async () => { await AngelesInpalAsync(); },
                    () => CanReload()
                ));
            }
        }

        Command _villalobonfilterCommand;
        public Command villalobonFilter

        {
            get
            {
                return _villalobonfilterCommand ?? (_villalobonfilterCommand = new Command(
                    async () => { await VillalobonInpalAsync(); },
                    () => CanReload()
                ));
            }
        }

        Command _agrariaFilterCommand;
        public Command AgrariaFilterCommand

        {
            get
            {
                return _agrariaFilterCommand ?? (_agrariaFilterCommand = new Command(
                    async () => { await AgrariaFilterAsync(); },
                    () => CanReload()
                ));
            }
        }

        Command _edificacionFilterCommand;
        public Command EdificacionFilterCommand

        {
            get
            {
                return _edificacionFilterCommand ?? (_edificacionFilterCommand = new Command(
                    async () => { await EdificacionFilterAsync(); },
                    () => CanReload()
                ));
            }
        }

        Command _fabricacionFilterCommand;
        public Command FabricacionFilterCommand

        {
            get
            {
                return _fabricacionFilterCommand ?? (_fabricacionFilterCommand = new Command(
                    async () => { await FabricacionFilterAsync(); },
                    () => CanReload()
                ));
            }
        }
        Command _deportivaFilterCommand;
        public Command DeportivaFilterCommand

        {
            get
            {
                return _deportivaFilterCommand ?? (_deportivaFilterCommand = new Command(
                    async () => { await DeportivaFilterAsync(); },
                    () => CanReload()
                ));
            }
        }
        Command _gestionFilterCommand;
        public Command GestionFilterCommand

        {
            get
            {
                return _gestionFilterCommand ?? (_gestionFilterCommand = new Command(
                    async () => { await GestionFilterAsync(); },
                    () => CanReload()
                ));
            }
        }
        Command _graficasFilterCommand;
        public Command GraficasFilterCommand

        {
            get
            {
                return _graficasFilterCommand ?? (_graficasFilterCommand = new Command(
                    async () => { await GraficasFilterAsync(); },
                    () => CanReload()
                ));
            }
        }
        Command _artesFilterCommand;
        public Command ArtesFilterCommand

        {
            get
            {
                return _artesFilterCommand ?? (_artesFilterCommand = new Command(
                    async () => { await ArtesFilterAsync(); },
                    () => CanReload()
                ));
            }
        }
        Command _comercioFilterCommand;
        public Command ComercioFilterCommand

        {
            get
            {
                return _comercioFilterCommand ?? (_comercioFilterCommand = new Command(
                    async () => { await ComercioFilterAsync(); },
                    () => CanReload()
                ));
            }
        }
        Command _electricidadFilterCommand;
        public Command ElectricidadFilterCommand

        {
            get
            {
                return _electricidadFilterCommand ?? (_electricidadFilterCommand = new Command(
                    async () => { await ElectricidadFilterAsync(); },
                    () => CanReload()
                ));
            }
        }

        Command _energiaFilterCommand;
        public Command EnergiaFilterCommand

        {
            get
            {
                return _energiaFilterCommand ?? (_energiaFilterCommand = new Command(
                    async () => { await EnergiaFilterAsync(); },
                    () => CanReload()
                ));
            }
        }
        Command _hosteleriaFilterCommand;
        public Command HosteleriaFilterCommand

        {
            get
            {
                return _hosteleriaFilterCommand ?? (_hosteleriaFilterCommand = new Command(
                    async () => { await HosteleriaFilterAsync(); },
                    () => CanReload()
                ));
            }
        }
        Command _personalFilterCommand;
        public Command PersonalFilterCommand

        {
            get
            {
                return _personalFilterCommand ?? (_personalFilterCommand = new Command(
                    async () => { await PersonalFilterAsync(); },
                    () => CanReload()
                ));
            }
        }
        Command _imagenFilterCommand;
        public Command ImagenFilterCommand

        {
            get
            {
                return _imagenFilterCommand ?? (_imagenFilterCommand = new Command(
                    async () => { await ImagenFilterAsync(); },
                    () => CanReload()
                ));
            }
        }
        Command _alimentariasFilterCommand;
        public Command AlimentariasFilterCommand

        {
            get
            {
                return _alimentariasFilterCommand ?? (_alimentariasFilterCommand = new Command(
                    async () => { await AlimentariasFilterAsync(); },
                    () => CanReload()
                ));
            }
        }
        Command _extractivasFilterCommand;
        public Command ExtractivasFilterCommand

        {
            get
            {
                return _extractivasFilterCommand ?? (_extractivasFilterCommand = new Command(
                    async () => { await ExtractivasFilterAsync(); },
                    () => CanReload()
                ));
            }
        }
        Command _informaticaFilterCommand;
        public Command InformaticaFilterCommand

        {
            get
            {
                return _informaticaFilterCommand ?? (_informaticaFilterCommand = new Command(
                    async () => { await InformaticaFilterAsync(); },
                    () => CanReload()
                ));
            }
        }
        Command _mantenimientoFilterCommand;
        public Command MantenimientoFilterCommand

        {
            get
            {
                return _mantenimientoFilterCommand ?? (_mantenimientoFilterCommand = new Command(
                    async () => { await MantenimientoFilterAsync(); },
                    () => CanReload()
                ));
            }
        }
        Command _maderaFilterCommand;
        public Command MaderaFilterCommand

        {
            get
            {
                return _maderaFilterCommand ?? (_maderaFilterCommand = new Command(
                    async () => { await MaderaFilterAsync(); },
                    () => CanReload()
                ));
            }
        }
        Command _pesqueraFilterCommand;
        public Command PesqueraFilterCommand

        {
            get
            {
                return _pesqueraFilterCommand ?? (_pesqueraFilterCommand = new Command(
                    async () => { await PesqueraFilterAsync(); },
                    () => CanReload()
                ));
            }
        }
        Command _químicaFilterCommand;
        public Command QuímicaFilterCommand

        {
            get
            {
                return _químicaFilterCommand ?? (_químicaFilterCommand = new Command(
                    async () => { await QuímicaFilterAsync(); },
                    () => CanReload()
                ));
            }
        }
        Command _sanidadFilterCommand;
        public Command SanidadFilterCommand

        {
            get
            {
                return _sanidadFilterCommand ?? (_sanidadFilterCommand = new Command(
                    async () => { await SanidadFilterAsync(); },
                    () => CanReload()
                ));
            }
        }
        Command _seguridadFilterCommand;
        public Command SeguridadFilterCommand

        {
            get
            {
                return _seguridadFilterCommand ?? (_seguridadFilterCommand = new Command(
                    async () => { await SeguridadFilterAsync(); },
                    () => CanReload()
                ));
            }
        }
        Command _socioculturalesFilterCommand;
        public Command SocioculturalesFilterCommand

        {
            get
            {
                return _socioculturalesFilterCommand ?? (_socioculturalesFilterCommand = new Command(
                    async () => { await SocioculturalesFilterAsync(); },
                    () => CanReload()
                ));
            }
        }
        Command _textilFilterCommand;
        public Command TextilFilterCommand

        {
            get
            {
                return _textilFilterCommand ?? (_textilFilterCommand = new Command(
                    async () => { await TextilFilterAsync(); },
                    () => CanReload()
                ));
            }
        }
        Command _transporteFilterCommand;
        public Command TransporteFilterCommand

        {
            get
            {
                return _transporteFilterCommand ?? (_transporteFilterCommand = new Command(
                    async () => { await TransporteFilterAsync(); },
                    () => CanReload()
                ));
            }
        }
        Command _vidrioFilterCommand;
        public Command VidrioFilterCommand

        {
            get
            {
                return _vidrioFilterCommand ?? (_vidrioFilterCommand = new Command(
                    async () => { await VidrioFilterAsync(); },
                    () => CanReload()
                ));
            }
        }

        Command _changeFilterCommand;
        public Command ChangeFilterCommand
        {
            get
            {
                return _changeFilterCommand ?? (_changeFilterCommand = new Command(
                    async () => { await FilterButtonAsync(); },
                    () => CanReload()
                ));
            }
        }
        
        #endregion
    }
}
