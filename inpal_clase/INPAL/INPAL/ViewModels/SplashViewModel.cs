﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Stormlion.SNavigation;

namespace INPAL
{
    public class SplashViewModel : BaseViewModel
    {
        #region Constructors

        public SplashViewModel()
        {

        }

        #endregion

        #region Properties

        #endregion

        #region Functions
        private async Task GetINPALFBAsync()
        {

                //Traemos la BD remota
                List<INPAL> companies = await RestManager.Instance.GetINPALList();

                if (companies != null)
                {
                //Persistimos/guardamos en BDlocal-SQLite la lista de compañias obtenidas del servidor
                // Borrar la BD de SQLite
                 await DataManager.Instance.DeleteSQLiteyAsync();
                    // Persistir
                    foreach (INPAL company in companies)
                    {
                        await DataManager.Instance.SaveINPALAsync(company);
                    }

                }
        }       

        private async Task ExecuteLoadInfoAsync()
        {
            using (new Busy(this))
            {
                //Si hay conexion: compruebo si hay que sincronizar
                if (Online == true)
                {
                    //Obtener fechas
                    //1.- Traer fecha de la BD remota: fechaFB
                    List<Fecha> fechaFB = await RestManager.Instance.GetFechaList();

                    //2.- Traer fecha de BD local: fechaSQLite
                    List<Fecha> fechaSQLite = await DataManager.Instance.GetFechaAsync();

                    //Si las fechas son distintas se sincroniza
                    if (fechaFB[0].F != fechaSQLite[0].F)
                    {
                        await GetINPALFBAsync();
                        //Actualizar fechaSQLite al valor de fechaFB
                        await DataManager.Instance.DeleteFechaAsync();
                        await DataManager.Instance.SaveFechaAsync(fechaFB[0]);
                    }
                }
                //Si no hay conexion: notifico
                else
                {
                    MessagingCenter.Send<SplashViewModel>(this, Constants.notif_conexion);
                }
                Application.Current.MainPage = new SNavigationPage(new MainPage());
            }
        }

        #endregion

        #region Commands

        Command _loadInfoCommand;
        public Command LoadInfoCommand

        {
            get
            {
                return _loadInfoCommand ?? (_loadInfoCommand = new Command(
                    async () => { await ExecuteLoadInfoAsync(); },
                    () => CanReload()
                ));
            }
        }

        #endregion

    }
}
