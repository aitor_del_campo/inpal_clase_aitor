﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;
using System.ComponentModel;
using Plugin.Connectivity;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Net.Http;


namespace INPAL
{
    public class BaseViewModel : BaseNotify
    {
        public BaseViewModel()
        {
            Initialize();
            Page = null;
        }

        public BaseViewModel(ContentPage page)
        {
            Initialize();
            Page = page;
        }
        public BaseViewModel(TabbedPage page)
        {
            Initialize();
            PageT = page;
        }


       

        #region Properties

        public ContentPage Page
        {
            get;
            set;
        }
        public TabbedPage PageT
        {
            get;
            set;
        }

        private bool _isBusy = false;

        public bool IsBusy
        {
            get
            {
                return _isBusy;
            }
            set
            {
                _isBusy = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("IsNotBusy");
            }
        }

        public bool IsNotBusy
        {
            get { return !IsBusy; }
        }

        private bool _landscape;
        public bool Landscape
        {
            get { return _landscape; }

            set
            {
                _landscape = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("Portrait");
            }
        }

        public bool Portrait
        {
            get { return !Landscape; }
        }

        #endregion

        #region conexion
        public bool Online
        {
            get
            {
                return CrossConnectivity.Current.IsConnected;
            }
        }
        public bool Offline
        {
            get
            {
                return !Online;
            }
        }
        #endregion


        public bool CanReload()
        {
            return IsNotBusy;
        }

        protected void SetProperty<T>(
        ref T backingStore, T value,
        string propertyName = "",
        Action onChanged = null,
        Action<T> onChanging = null)
        {


            if (EqualityComparer<T>.Default.Equals(backingStore, value))
                return;
            if (onChanging != null)
                onChanging(value);

            OnPropertyChanging(propertyName);
            backingStore = value;

            if (onChanged != null)
                onChanged();

            OnPropertyChanged(propertyName);
        }


        #region INotifyPropertyChanging implementation
        public event Xamarin.Forms.PropertyChangingEventHandler PropertyChanging;
        #endregion

        public void OnPropertyChanging(string propertyName)
        {
            if (PropertyChanging == null)
                return;

            PropertyChanging(this, new Xamarin.Forms.PropertyChangingEventArgs(propertyName));
        }

        #region INotifyPropertyChanged implementation
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged == null)
                return;

            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Initialize()
        {
            CrossConnectivity.Current.ConnectivityChanged += (sender, e) =>
            {
                NotifyPropertyChanged("Online");
            };
        }

        public async Task RunSafe(Func<Task> execute, bool notifyOnError = false)
        {
            Exception networkException = null;

            if (!CrossConnectivity.Current.IsConnected)
            {
                MessagingCenter.Send<BaseViewModel>(this, Constants.notif_conexion);
                return;
            }

            try
            {
                await execute();
            }
            catch (TaskCanceledException ex)
            {
                Debug.WriteLine("Task Cancelled");

            }
            catch (HttpRequestException ex)
            {
                networkException = ex;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"Exception handled {0}", ex.Message);
            }
            finally
            {
                if (networkException != null)
                {
                    Debug.WriteLine(string.Format("SERVER NOT REACHABLE {0}", networkException.Message));

                    if (notifyOnError)
                    {
                        MessagingCenter.Send<BaseViewModel>(this, Constants.notif_servernotreachable);
                    }
                }
            }
        }
    }
}
