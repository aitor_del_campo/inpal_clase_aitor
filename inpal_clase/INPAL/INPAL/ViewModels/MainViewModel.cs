﻿using Stormlion.SNavigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;


namespace INPAL
{
    public class MainViewModel : BaseViewModel
    {
        #region Constructors
        public MainViewModel()
        {

        }

        #endregion

        #region Properties 
        #endregion

        #region Function
        
        private async Task ExecuteNavigateToSearchPageAsync()
        {
            await Page.Navigation.PushModalAsync(new SNavigationPage(new SearchPage()));
        }

        private async Task ExecuteNavigateToMapPageAsync()
        {
            await Page.Navigation.PushAsync(new MapPage());
        }

        private async Task ExecuteNavigateToContactPageAsync()
        {
            await Page.Navigation.PushAsync(new ContactPage());
        }


        #endregion

        #region Command
        
        Command _navigateToSearchPageCommand;
        public Command NavigateToSearchPageCommand

        {
            get
            {
                return _navigateToSearchPageCommand ?? (_navigateToSearchPageCommand = new Command(
                    async () => { await ExecuteNavigateToSearchPageAsync(); },
                    () => CanReload()
                ));
            }
        }

        Command _navigateToMapPageCommand;
        public Command NavigateToMapPageCommand

        {
            get
            {
                return _navigateToMapPageCommand ?? (_navigateToMapPageCommand = new Command(
                    async () => { await ExecuteNavigateToMapPageAsync(); },
                    () => CanReload()
                ));
            }
        }

        Command _navigateToContactPageCommand;
        public Command NavigateToContactPageCommand

        {
            get
            {
                return _navigateToContactPageCommand ?? (_navigateToContactPageCommand = new Command(
                    async () => { await ExecuteNavigateToContactPageAsync(); },
                    () => CanReload()
                ));
            }
        }



        #endregion

    }
    
}
