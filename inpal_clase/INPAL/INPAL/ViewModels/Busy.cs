﻿using System;
using System.Collections.Generic;
using System.Text;

namespace INPAL
{
    public class Busy : IDisposable
    {
        readonly object _sync = new Object();
        readonly BaseViewModel _viewModel;

        public Busy(BaseViewModel viewModel)
        {
            _viewModel = viewModel;
            lock (_sync)
            {
                _viewModel.IsBusy = true;
            }
        }

        public void Dispose()
        {
            lock (_sync)
            {
                _viewModel.IsBusy = false;
            }
        }
    }
}
