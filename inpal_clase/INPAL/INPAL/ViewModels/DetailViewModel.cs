﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace INPAL
{
    
    public class DetailViewModel : BaseViewModel
    {
        #region Constructors
        public DetailViewModel()
        {

        }

        #endregion

        #region Properties

        private INPAL _inpal;
        public INPAL Inpal
        {
            get
            {
                return _inpal;
            }
            set
            {
                _inpal = value;
                NotifyPropertyChanged();
            }
        }

        private Map _map;
        public Map Map
        {
            get
            {
                return _map;
            }
            set
            {
                _map = value;
                NotifyPropertyChanged();
            }
        }

        #endregion

        #region Function

        public void Initialize(INPAL inpal)
        {
            using (new Busy(this))
            {
                Inpal = inpal;
                PositionMap();
                AddPin();
            }
        }

        private void PositionMap()
        {
            float latitude = Inpal.Latitude != 0 ? Inpal.Latitude : (float)Constants.LatitudeP;
            float longitude = Inpal.Length != 0 ? Inpal.Length : (float)Constants.LenghtP;

            Map.MoveToRegion(
                 MapSpan.FromCenterAndRadius(
                      new Position(latitude, longitude),
                      Distance.FromMeters(Constants.MAPS_DEFAULT_CENTER_DISTANCE_UNIQUE_POI)));
        }

        private void AddPin()
        {
            var pin = new Pin
            {
                Type = PinType.Generic,
                Position = new Position(Inpal.Latitude, Inpal.Length),
                Label = Inpal.Name
            };

            Map.Pins.Add(pin);            
        }

        private async Task GoFacebookAsync()
        {
            using (new Busy(this))
            {
                if (!string.IsNullOrEmpty(Inpal.Facebook))
                {
                    Device.OpenUri(new System.Uri(Inpal.Facebook));
                }

            }

        }
        private async Task GoLinkedinAsync()
        {
            using (new Busy(this))
            {
                if (!string.IsNullOrEmpty(Inpal.Linkedin))
                {
                    Device.OpenUri(new System.Uri(Inpal.Linkedin));
                }

            }

        }
        private async Task GoTwitterAsync()
        {
            using (new Busy(this))
            {
                if (!string.IsNullOrEmpty(Inpal.Twitter))
                {
                    Device.OpenUri(new System.Uri(Inpal.Twitter));
                }

            }

        }
       private async Task GoWebAsync()
        {
            using (new Busy(this))
            {
                if (!string.IsNullOrEmpty(Inpal.Web))
                {
                    Device.OpenUri(new System.Uri(Inpal.Web));
                }

            }

        }
        private async Task GoTlf1Async()
        {
            using (new Busy(this))
            {
                if (!string.IsNullOrEmpty(Inpal.Tlf1))
                {
                    Device.OpenUri(new System.Uri("tel:" + Inpal.Tlf1));
                }

            }

       }
        private async Task GoTlf2Async()
        {
            using (new Busy(this))
            {
                if (!string.IsNullOrEmpty(Inpal.Tlf2))
                {
                    Device.OpenUri(new System.Uri("tel:" + Inpal.Tlf2));
                }

            }

        }
        private async Task GoEmailAsync()
        {
            using (new Busy(this))
            {
                if (!string.IsNullOrEmpty(Inpal.Email))
                {
                    Device.OpenUri(new System.Uri("mailto:" + Inpal.Email));
                }

            }

        }

        #endregion

        #region Command

        Command _goFacebookComand;
        public Command GoFacebookCommand

        {
            get
            {
                return _goFacebookComand ?? (_goFacebookComand = new Command(
                    async () => { await GoFacebookAsync(); },
                    () => CanReload()
                ));
            }
        }
        Command _goLinkedinComand;
        public Command GoLinkedinCommand

        {
            get
            {
                return _goLinkedinComand ?? (_goLinkedinComand = new Command(
                    async () => { await GoLinkedinAsync(); },
                    () => CanReload()
                ));
            }
        }
        Command _goTwitterComand;
        public Command GoTwitterCommand

        {
            get
            {
                return _goTwitterComand ?? (_goTwitterComand = new Command(
                    async () => { await GoTwitterAsync(); },
                    () => CanReload()
                ));
            }
        }
        Command _goWebComand;
        public Command GoWebCommand

        {
            get
            {
                return _goWebComand ?? (_goWebComand = new Command(
                    async () => { await GoWebAsync(); },
                    () => CanReload()
                ));
            }
        }
        Command _goTlf1Command;
        public Command GoTlf1Command

        {
           get
            {
                return _goTlf1Command ?? (_goTlf1Command = new Command(
                    async () => { await GoTlf1Async(); },
                    () => CanReload()
                ));
            }
        }
        Command _goTlf2Command;
        public Command GoTlf2Command
 
        {
            get
            {
                return _goTlf2Command ?? (_goTlf2Command = new Command(
                    async () => { await GoTlf2Async(); },
                    () => CanReload()
                ));
            }
        }
        Command _goEmailCommand;
        public Command GoEmailCommand

        {
            get
            {
                return _goEmailCommand ?? (_goEmailCommand = new Command(
                    async () => { await GoEmailAsync(); },
                    () => CanReload()
                ));
            }
        }

        #endregion

    }
}


