﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INPAL
{
    public class Constants
    {
        //public const string BasePath = "https://fb10-a43dd.firebaseio.com/";
        public const string BasePath = "https://primera-prueba-52d96.firebaseio.com/";

        public static string URL_GET_COMPANIES = "INPAL";

        //Maps
        public static readonly double MAPS_DEFAULT_CENTER_DISTANCE_UNIQUE_POI = 300;

        public static readonly string Angeles = "Ntra. Señora de los Ángeles";
        public static readonly string Antolin = "San Antolín";
        public static readonly string Poligonillo = "Poligonillo";
        public static readonly string Villalobon = "Villalobón";
       
        //Literales Actividades
        public static readonly string Deportiva = "Actividades fisicas y deportivas";
        public static readonly string Gestion = "Administracion y gestion";
        public static readonly string Graficas = "Artes gráficas";
        public static readonly string Artes = "Artes y artesanias";
        public static readonly string Agraria = "Agraria";
        public static readonly string Edificacion = "Edificación y obra civil";
        public static readonly string Fabricacion = "Fabricación mecánica";
        public static readonly string Comercio = "Comercio y marketing";
        public static readonly string Electricidad = "Electricidad y electrónica";
        public static readonly string Energia = "Energía y agua";
        public static readonly string Hosteleria = "Hostelería y turismo";
        public static readonly string Personal = "Imagen personal";
        public static readonly string Imagen = "Imagen y sonido";
        public static readonly string Alimentarias = "Industrias alimentarias";
        public static readonly string Extractivas = "Industrias Extractivas";
        public static readonly string Informatica = "Informática y comunicaciones";
        public static readonly string Mantenimiento = "Instalación y mantenimiento";
        public static readonly string Madera = "Madera, mueble y corcho";
        public static readonly string Pesquera = "Marítimo persquera";
        public static readonly string Química = "Química";
        public static readonly string Sanidad = "Sanidad";
        public static readonly string Seguridad = "Seguridad y medio ambiente";
        public static readonly string Socioculturales = "Servicios socioculturales y a la comunidad";
        public static readonly string Textil = "Textil, confección y piel";
        public static readonly string Transporte = "Transporte y mantenimiento de vehículos";
        public static readonly string Vidrio = "Vidrio y cerámica";
        //conexion
        public static readonly string notif_conexion = "notif_conexion";
        public static readonly string notif_servernotreachable = "notif_servernotreachable";

        //Coordenadas por defecto Palencia
        public static readonly double LatitudeP = 42.009444;
        public static readonly double LenghtP = -4.529529;
    }
}