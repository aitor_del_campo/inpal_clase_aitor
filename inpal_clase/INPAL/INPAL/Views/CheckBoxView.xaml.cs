﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace INPAL
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CheckBoxView : ContentView
	{
        public static readonly BindableProperty TextProperty =
    BindableProperty.Create(
        "Text",
        typeof(string),
        typeof(CheckBoxView),
        null,
        propertyChanged: (bindable, oldValue, newValue) =>
        {
            ((CheckBoxView)bindable).textLabel.Text = (string)newValue;
        });

        public static readonly BindableProperty FontSizeProperty =
            BindableProperty.Create(
                "FontSize",
                typeof(double),
                typeof(CheckBoxView),
                Device.GetNamedSize(NamedSize.Default, typeof(Label)),
                propertyChanged: (bindable, oldValue, newValue) =>
                {
                    CheckBoxView checkbox = (CheckBoxView)bindable;
                    checkbox.boxLabel.FontSize = (double)newValue;
                    checkbox.textLabel.FontSize = (double)newValue;
                });

        public static readonly BindableProperty IsCheckedProperty =
            BindableProperty.Create(
                "IsChecked", typeof(bool),
                typeof(CheckBoxView),
                false,
                BindingMode.TwoWay,
                propertyChanged: (bindable, oldValue, newValue) =>
                {
                    // Set the graphic.
                    CheckBoxView checkbox = (CheckBoxView)bindable;
                    checkbox.boxLabel.Text = (bool)newValue ? "\u2611" : "\u2610";                    
                    // Fire the event.
                    checkbox.CheckedChanged?.Invoke(checkbox, (bool)newValue);
                });

        public event EventHandler<bool> CheckedChanged;

        public CheckBoxView()
        {
            InitializeComponent();
        }

        public string Text
        {
            set { SetValue(TextProperty, value); }
            get { return (string)GetValue(TextProperty); }
        }

        [TypeConverter(typeof(FontSizeConverter))]
        public double FontSize
        {
            set { SetValue(FontSizeProperty, value); }
            get { return (double)GetValue(FontSizeProperty); }
        }

        public bool IsChecked
        {
            set { SetValue(IsCheckedProperty, value); }
            get { return (bool)GetValue(IsCheckedProperty); }
        }

        // TapGestureRecognizer handler.
        void OnCheckBoxTapped(object sender, EventArgs args)
        {
            IsChecked = !IsChecked;            
        }
    }
}