﻿using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace INPAL
{
    [Table("Fecha")]
    public class Fecha : BaseModel
    {
        private string _f;
        public string F
        {
            get { return _f; }
            set
            {
                _f = value;
                NotifyPropertyChanged();
            }
        }
    }
}
