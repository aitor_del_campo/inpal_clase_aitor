﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite.Net.Attributes;

namespace INPAL
{
    public class BaseModel : BaseNotify
    {
        int _id;

        //[AutoIncrement]
        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
                NotifyPropertyChanged();
            }
        }
    }
}