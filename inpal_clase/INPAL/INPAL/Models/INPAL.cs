﻿using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System.Collections;
using System.Collections.Generic;


namespace INPAL
{

    [Table("INPAL")]
    public class INPAL : BaseModel
    {

        private string _marca_temporal;

        public string Marca_Temporal
        {
            get { return _marca_temporal; }
            set
            {
                _marca_temporal = value;
                NotifyPropertyChanged();
            }

        }
        private string _email;

        public string Email
        {
            get
            {
                return _email;
                ;
            }
            set
            {
                _email = value;
                NotifyPropertyChanged();
            }

        }

        private string _name;

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                NotifyPropertyChanged();
            }

        }
        private string _cif_nif;
        public string CIF_NIF
        {
            get { return _cif_nif; }
            set
            {
                _cif_nif = value;
                NotifyPropertyChanged();
            }

        }
        private string _logo;
        public string Logo
        {
            get { return _logo; }
            set
            {
                _logo = value;
                NotifyPropertyChanged();
            }

        }
        private string _faceebook;
        public string Facebook
        {
            get { return _faceebook; }
            set
            {
                _faceebook = value;
                NotifyPropertyChanged();
            }

        }

        private string _twitter;
        public string Twitter
        {
            get { return _twitter; }
            set
            {
                _twitter = value;
                NotifyPropertyChanged();
            }

        }
        private string _linkedin;
        public string Linkedin
        {
            get { return _linkedin; }
            set
            {
                _linkedin = value;
                NotifyPropertyChanged();
            }

        }
        private string _instagram;
        public string Instagram
        {
            get { return _instagram; }
            set
            {
                _instagram = value;
                NotifyPropertyChanged();
            }

        }
        private string _web;
        public string Web
        {
            get { return _web; }
            set
            {
                _web = value;
                NotifyPropertyChanged();
            }

        }
        private string _blog;
        public string Blog
        {
            get { return _blog; }
            set
            {
                _blog = value;
                NotifyPropertyChanged();
            }

        }
        private string _poligon;
        public string Polygon
        {
            get { return _poligon; }
            set
            {
                _poligon = value;
                NotifyPropertyChanged();
            }

        }
        private string _adress;
        public string Address
        {
            get { return _adress; }
            set
            {
                _adress = value;
                NotifyPropertyChanged();
            }

        }
        private string _cp;
        public string CP
        {
            get { return _cp; }
            set
            {
                _cp = value;
                NotifyPropertyChanged();
            }

        }
        private string _tlf1;
        public string Tlf1
        {
            get { return _tlf1; }
            set
            {
                _tlf1 = value;
                NotifyPropertyChanged();
            }

        }
        private string _tlf2;
        public string Tlf2
        {
            get { return _tlf2; }
            set
            {
                _tlf2 = value;
                NotifyPropertyChanged();
            }

        }
        private string _timetable;
        public string Timetable
        {
            get { return _timetable; }
            set
            {
                _timetable = value;
                NotifyPropertyChanged();
            }

        }
        private float _latitude = 0;
        public float Latitude
        {
            get { return _latitude; }
            set
            {
                _latitude = value;
                NotifyPropertyChanged();
            }

        }
        private float _length = 0;
        public float Length
        {
            get { return _length; }
            set
            {
                _length = value;
                NotifyPropertyChanged();
            }

        }
        private string _description;
        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
                NotifyPropertyChanged();
            }

        }
        //es jpg
        private string _doorImage;
        public string Doorimage

        {
            get { return _doorImage; }
            set
            {
                _doorImage = value;
                NotifyPropertyChanged();
            }
        }

        private string _activity;
        public string Activity
        {
            get { return _activity; }
            set
            {
                _activity = value;
                NotifyPropertyChanged();
            }
        }

        private string _otherActivity;
        public string Other_activity
        {
            get { return _otherActivity; }
            set
            {
                _otherActivity = value;
                NotifyPropertyChanged();
            }
        }

        private string _accept;
        public string Accept
        {
            get { return _accept; }
            set
            {
                _accept = value;
                NotifyPropertyChanged();
            }
        }

        private string _comunicacionesInformativas;
        public string COMUNICACIONES_INFORMATIVAS
        {
            get { return _comunicacionesInformativas; }
            set
            {
                _comunicacionesInformativas = value;
                NotifyPropertyChanged();
            }
        }

    }
}



