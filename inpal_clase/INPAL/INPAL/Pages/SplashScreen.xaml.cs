﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace INPAL
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SplashScreen : SplashScreenXaml
    {
        public SplashScreen()
        {
            InitializeComponent();            
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            ViewModel.LoadInfoCommand.Execute(null);
        }
    }
    public partial class SplashScreenXaml : BaseContentPage<SplashViewModel>
    {

    }
}