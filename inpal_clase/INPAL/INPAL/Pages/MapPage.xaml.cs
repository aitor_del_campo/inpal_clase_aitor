﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace INPAL
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MapPage : MapPageXaml
    {
        public MapPage()
        {
            InitializeComponent();
        }
    }
    public partial class MapPageXaml : BaseContentPage<MapViewModel>
    {

    }

}