﻿using Xamarin.Forms;

namespace INPAL
{
    public class BaseTabbedPage<T> : TabbedPage where T : BaseViewModel, new()
    {
        double width;
        double height;

        protected T _viewModel;

        public T ViewModel
        {
            get
            {
                if (_viewModel == null)
                {
                    _viewModel = new T();
                    _viewModel.PageT = this;
                }

                return _viewModel;
            }
        }


        ~BaseTabbedPage()
        {
            _viewModel = null;
        }

        public BaseTabbedPage()
        {
            BindingContext = ViewModel;
        }

        #region Functions

        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);
            if (width != this.width || height != this.height)
            {
                this.width = width;
                this.height = height;
                if (width > height)
                {
                    ViewModel.Landscape = true;
                }
                else
                {
                    ViewModel.Landscape = false;
                }
            }
        }

        #endregion
    }
}