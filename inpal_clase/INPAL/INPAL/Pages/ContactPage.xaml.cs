﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace INPAL
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ContactPage : ContactPageXaml
	{
		public ContactPage()
		{
			InitializeComponent();
		}
	}
    public partial class ContactPageXaml : BaseContentPage<ContactViewModel>
    {

    }

}