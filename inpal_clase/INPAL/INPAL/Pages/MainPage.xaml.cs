﻿using Stormlion.SNavigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace INPAL
{
	public partial class MainPage : MainPageXaml
	{
		public MainPage()
		{
			InitializeComponent();
            NavigationPage.SetHasNavigationBar(this,false);
		}
        
    }
    public partial class MainPageXaml : BaseContentPage<MainViewModel>
    {

    }

}
