﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace INPAL
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SearchPage : SearchPageXaml
    {
        public SearchPage ()
        {
            InitializeComponent();
            SearchBar.TextChanged += SearchBar_TextChanged;
        }

        private void SearchBar_TextChanged(object sender, TextChangedEventArgs e)
        {
            ViewModel.ExecuteSearchAsync(e.NewTextValue);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            ViewModel.LoadInfoCommand.Execute(null);
            //ViewModel.LoadActivityCommand.Execute(null);
        }

        void OnButtonGoToMainPage(object sender, EventArgs args)
        {
            Navigation.PushAsync(new MainPage());
        }

        void OnItemSelected(object sender, ItemTappedEventArgs args)
        {
            var item = args.Item as INPAL;
            if (item == null)
            {
                return;
            }

            Navigation.PushAsync(new DetailPage(item));            
        }
    }
    public partial class SearchPageXaml : BaseTabbedPage<SearchViewModel>
    {

    }
}