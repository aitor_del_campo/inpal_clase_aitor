﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms.Maps;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace INPAL
{	
	public partial class DetailPage : DetailPageXaml
	{
        public DetailPage(INPAL inpal)
        {
            InitializeComponent();
            ViewModel.Map = DetailMap;
            ViewModel.Initialize(inpal);
        }
    }
    public partial class DetailPageXaml : BaseContentPage<DetailViewModel>
    {

    }
}