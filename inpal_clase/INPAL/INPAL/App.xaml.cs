﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;
using Stormlion.SNavigation;

namespace INPAL
{
	public partial class App : Application
	{
        public static bool MOCKED_BACKEND = false;

        public App ()
		{
			InitializeComponent();

            MainPage = new SNavigationPage(new SplashScreen());

            ToastService.Instance.Init();
            
            //Se elige si un servicio moqueado o real
            if (MOCKED_BACKEND)
            {
                DependencyService.Register<IRestServices, MockRestServices>();
            }
            else
            {
                DependencyService.Register<IRestServices, RestServices>();
            }

            //MainPage = new INPAL.MainPage();
            
            MainPage = new SplashScreen();

		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
