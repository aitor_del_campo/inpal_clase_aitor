﻿using System;
using SQLite.Net;
using SQLite.Net.Async;

namespace INPAL
{
    public interface ISQLite
    {
        SQLiteAsyncConnection GetAsyncConnection();
        SQLiteConnection GetConnection();
    }
}
