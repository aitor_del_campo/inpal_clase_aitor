﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace INPAL
{
    public interface IRestServices
    {
        Task<List<INPAL>> GetINPALListAsync();
        Task<List<Fecha>> GetFechaAsync();
    }
}