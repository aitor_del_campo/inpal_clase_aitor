﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace INPAL
{
    class AllButtonTextConverter : IValueConverter
    {
        private String IsAllTextOn = "Seleccionar todo";
        private String IsAllTextOff = "Quitar todo";

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is bool)) return IsAllTextOff;
            
            return (bool)value ? IsAllTextOn : IsAllTextOff;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
