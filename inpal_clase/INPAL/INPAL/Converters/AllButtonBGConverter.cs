﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace INPAL
{
    class AllButtonBGConverter : IValueConverter
    {
        private Color IsAllOn = (Color)App.Current.Resources["colorPrimary"];
        private Color IsAllOff = (Color)App.Current.Resources["secundaryColor"];

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is bool)) return IsAllOff;

            //Si value es true--> cambia el color a PoligonilloOn. En caso contrario establece el color a PoligonilloOff
            return (bool)value ? IsAllOn : IsAllOff;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
