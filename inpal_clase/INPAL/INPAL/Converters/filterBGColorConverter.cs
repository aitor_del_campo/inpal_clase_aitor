﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace INPAL
{
    class filterBGColorConverter : IValueConverter
    {
        private Color IsPruebaOn = (Color)App.Current.Resources["colorPrimary"];
        private Color IsPruebaOff = (Color)App.Current.Resources["colorActivityFilterClear"];

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is bool)) return IsPruebaOff;

            //Si value es true--> cambia el color a PoligonilloOn. En caso contrario establece el color a PoligonilloOff
            return (bool)value ? IsPruebaOn : IsPruebaOff;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}

