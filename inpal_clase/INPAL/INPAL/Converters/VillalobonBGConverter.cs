﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace INPAL
{
    class VillalobonBGConverter : IValueConverter
    {
        private Color VillalobonOn = (Color)App.Current.Resources["Villalobon_color_ON"];
        private Color VillalobonOff = (Color)App.Current.Resources["Villalobon_color"];

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is bool)) return VillalobonOff;

            //Si value es true--> cambia el color a PoligonilloOn. En caso contrario establece el color a PoligonilloOff
            return (bool)value ? VillalobonOn : VillalobonOff;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
