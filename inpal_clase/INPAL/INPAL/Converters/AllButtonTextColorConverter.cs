﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace INPAL
{
    class AllButtonTextColorConverter : IValueConverter
    {
        private Color IsAllTextColorOn = (Color)App.Current.Resources["secundaryColor"];
        private Color IsAllTextColorOff = (Color)App.Current.Resources["colorPrimary"];

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is bool)) return IsAllTextColorOff;

            //Si value es true--> cambia el color a PoligonilloOn. En caso contrario establece el color a PoligonilloOff
            return (bool)value ? IsAllTextColorOn : IsAllTextColorOff;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
