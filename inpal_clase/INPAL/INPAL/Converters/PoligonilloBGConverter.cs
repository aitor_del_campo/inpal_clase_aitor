﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace INPAL
{
    public class PoligonilloBGConverter : IValueConverter
    {
        //Definicion de colores

        private Color PoligonilloOn = (Color)App.Current.Resources["Poligonillo_color_ON"];
        private Color PoligonilloOff = (Color)App.Current.Resources["Poligonillo_color"];

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is bool)) return PoligonilloOff;

            //Si value es true--> cambia el color a PoligonilloOn. En caso contrario establece el color a PoligonilloOff
            return (bool)value ? PoligonilloOn : PoligonilloOff;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
