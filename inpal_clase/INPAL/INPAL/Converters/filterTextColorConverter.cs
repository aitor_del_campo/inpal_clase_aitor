﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace INPAL
{
    class filterTextColorConverter : IValueConverter
    {
        private Color IsFilterTextColorOn = (Color)App.Current.Resources["secundaryColor"];
        private Color IsFilterTextColorOff = (Color)App.Current.Resources["colorPrimary"];

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is bool)) return IsFilterTextColorOff;

            //Si value es true--> cambia el color a PoligonilloOn. En caso contrario establece el color a PoligonilloOff
            return (bool)value ? IsFilterTextColorOn : IsFilterTextColorOff;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
