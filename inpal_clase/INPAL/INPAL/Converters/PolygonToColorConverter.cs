﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace INPAL
{
    public class PolygonToColorConverter : IValueConverter
    {
        //Definicion de colores

        private Color Poligonillo = (Color)App.Current.Resources["Poligonillo_color"];
        private Color San_Antolin = (Color)App.Current.Resources["San_Antolin_color"];
        private Color Angeles = (Color)App.Current.Resources["Angeles_color"];
        private Color Villalobon = (Color)App.Current.Resources["Villalobon_color"];

        //Color por defecto 
        private Color SinPoligono = (Color)App.Current.Resources["SinPoligono_color"];

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is string)) return SinPoligono;

            string poligono = (string)value;
            if (poligono == "Poligonillo")
            {
                return Poligonillo;
            }
            else if (poligono == "San Antolín")
            {
                return San_Antolin;
            }
            else if (poligono == "Ntra. Señora de los Ángeles")
            {
                return Angeles;
            }
            else if (poligono == "Villalobón")
            {
                return Villalobon;
            }
            else
            {
                return SinPoligono;
            }

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
