﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace INPAL
{
    public class WebConverter : IValueConverter
    {
        private String webOn = "ic_web.png";
        private String webOff = "ic_webOff.png";

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is String)) return webOff;

            return !string.IsNullOrEmpty((string)value) ? webOn : webOff;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}