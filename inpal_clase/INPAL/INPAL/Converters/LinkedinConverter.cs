﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace INPAL
{
    public class LinkedinConverter : IValueConverter
    {
        private String linkedinOn = "ic_linkedin.png";
        private String linkedinOff = "ic_linkedinOff.png";

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is String)) return linkedinOff;

            return !string.IsNullOrEmpty((string)value) ? linkedinOn : linkedinOff;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
