﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace INPAL
{
    class AngelesBGConverter : IValueConverter
    {
        private Color AngelesOn = (Color)App.Current.Resources["Angeles_color_ON"];
        private Color AngelesOff = (Color)App.Current.Resources["Angeles_color"];

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is bool)) return AngelesOff;

            //Si value es true--> cambia el color a PoligonilloOn. En caso contrario establece el color a PoligonilloOff
            return (bool)value ? AngelesOn : AngelesOff;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
