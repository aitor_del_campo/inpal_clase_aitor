﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace INPAL
{
    public class FacebookConverter : IValueConverter
    {
        private String facebookOn = "ic_facebook.png";
        private String facebookOff = "ic_facebookOff.png";

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is String)) return facebookOff;

            return !string.IsNullOrEmpty((string)value) ? facebookOn : facebookOff;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}