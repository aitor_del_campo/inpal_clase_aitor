﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace INPAL
{
    class AntolinBGConverter : IValueConverter
    {
        private Color AntolinOn = (Color)App.Current.Resources["San_Antolin_color_ON"];
        private Color AntolinOff = (Color)App.Current.Resources["San_Antolin_color"];

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is bool)) return AntolinOff;

            //Si value es true--> cambia el color a PoligonilloOn. En caso contrario establece el color a PoligonilloOff
            return (bool)value ? AntolinOn : AntolinOff;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
