﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace INPAL
{
    public class TwitterConverter : IValueConverter
    {
        private String twitterOn = "ic_twitter.png";
        private String twitterOff = "ic_twitterOff.png";

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is String)) return twitterOff;

            return !string.IsNullOrEmpty((string)value) ? twitterOn : twitterOff;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
