﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using SQLite.Net.Async;
using Xamarin.Forms;

namespace INPAL
{
    public class DataManager
    {
        #region Properties

        static DataManager _instance;
        public static DataManager Instance
        {
            get
            {
                return _instance ?? (_instance = new DataManager());
            }
        }

        SQLiteAsyncConnection _database;

        bool _isInitialized;
        public bool IsInitialized { get { return _isInitialized; } }

        #endregion

        public async Task Init()
        {
            try
            {
                _database = DependencyService.Get<ISQLite>().GetAsyncConnection();
                await CreateTables();

                _isInitialized = true;

                String today = DateTime.Today.ToString();
                Fecha fecha = new Fecha()
                {
                    F = today
                };
                await SaveFechaAsync(fecha);
            }
            catch (Exception ex)
            {
                _isInitialized = false;
                Debug.WriteLine(@"ERROR {@}", ex.Message);
                MessagingCenter.Send<DataManager, Exception>(this, "ExceptionOccurred", ex);
            }
        }

        private async Task CreateTables()
        {
            try
            {
                await _database.CreateTableAsync<INPAL>();
                await _database.CreateTableAsync<Fecha>();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"ERROR {@}", ex.Message);
                MessagingCenter.Send<DataManager, Exception>(this, "ExceptionOccurred", ex);
            }
        }

        #region INPAL
        
        public async Task<bool> SaveINPALAsync(INPAL item)
        {
            try
            {
                if (!IsInitialized)
                {
                    await Init();
                }

                await _database.InsertAsync(item).ConfigureAwait(false);

                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"ERROR {@}", ex.Message);
                MessagingCenter.Send<DataManager, Exception>(this, "ExceptionOccurred", ex);
                return false;
            }
        }

        public async Task<bool> UpdateINPALAsync(INPAL item)
        {
            try
            {
                if (!IsInitialized)
                {
                    await Init();
                }

                await _database.UpdateAsync(item).ConfigureAwait(false);

                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"ERROR {@}", ex.Message);
                MessagingCenter.Send<DataManager, Exception>(this, "ExceptionOccurred", ex);
                return false;
            }
        }

        public async Task<INPAL> GetINPALByNameAsync(string name)
        {
            if (!IsInitialized)
            {
                await Init();
            }

            INPAL company = await _database.Table<INPAL>()
                                           .Where(x => x.Name == name)
                                           .FirstOrDefaultAsync()
                                           .ConfigureAwait(false);
            return company;
        }

        public async Task<INPAL> GetActivitiesINPALSQLAsync(string activity)
        {
            if (!IsInitialized)
            {
                await Init();
            }
            /*string SQLstring = string.Format("select distinct Activity from INPAL order by Activity asc");
            List<INPAL> result = await _database.QueryAsync<INPAL>(SQLstring); */
            INPAL result = await _database.Table<INPAL>()
                                        .Where(x => x.Activity == activity)
                                        .FirstOrDefaultAsync()
                                        .ConfigureAwait(false);
            return result;
        }

        public async Task<List<INPAL>> GetINPALByPolygonSQLAsync(
           bool IsAntolinOn, bool IsPoligonilloOn, bool IsVillalobonOn, bool IsAngelesOn,
           bool IsAgrariaOn, bool IsEdificacionOn, bool IsFabricacionOn, bool IsDeportivaOn,
           bool IsGestionOn, bool IsGraficasOn, bool IsArtesOn, bool IsComercioOn, bool IsElectricidadOn,
           bool IsEnergiaOn, bool IsHosteleriaOn, bool IsPersonalOn, bool IsImagenOn, bool IsAlimentariasOn,
           bool IsExtractivasOn, bool IsInformaticaOn, bool IsMantenimientoOn, bool IsMaderaOn, bool IsPesqueraOn,
           bool IsQuímicaOn, bool IsSanidadOn, bool IsSeguridadOn, bool IsSocioculturalesOn, bool IsTextilOn,
           bool IsTransporteOn, bool IsVidrioOn)
        {
            if (!IsInitialized)
            {
                await Init();
            }
            /*List<INPAL> inpalListAux = await DataManager.Instance.GetINPALAsync();
            List<INPAL> inpalList = null;

            int counter = 0;
            //string SQLstring = "select * from CompanyINPAL where Name='" + name + "'";
            //string SQLstring = string.Format("select * from INPAL where Polygon='{0}' order by Name asc", polygon);
            //List<INPAL> result = await _database.QueryAsync<INPAL>(SQLstring);
            foreach (INPAL item in inpalListAux)
            {
                INPAL inpal = await _database.Table<INPAL>()
                                               .Where(x => x.Polygon == polygon)
                                               .FirstOrDefaultAsync()
                                               .ConfigureAwait(false);

                //Guardar cada item del Poligono en la lista resultado
                if (inpal != null)
                {
                    inpalList[counter] = inpal;
                    counter = counter + 1;
                }
            }
            return inpalList;*/

            List<INPAL> resultMaster = new List<INPAL>();

            if (IsAntolinOn)
            {
                resultMaster = await GetINPALFilteredAsync(resultMaster, Constants.Antolin);
            }

            if (IsPoligonilloOn)
            {
                resultMaster = await GetINPALFilteredAsync(resultMaster, Constants.Poligonillo);
            }

            if (IsAngelesOn)
            {
                resultMaster = await GetINPALFilteredAsync(resultMaster, Constants.Angeles);
            }

            if (IsVillalobonOn)
            {
                resultMaster = await GetINPALFilteredAsync(resultMaster, Constants.Villalobon);
            }

            if (IsAgrariaOn && IsEdificacionOn && IsFabricacionOn && IsDeportivaOn
                && IsGestionOn && IsGraficasOn && IsArtesOn && IsComercioOn && IsElectricidadOn
                && IsEnergiaOn && IsHosteleriaOn && IsPersonalOn && IsImagenOn && IsAlimentariasOn
                && IsExtractivasOn && IsInformaticaOn && IsMantenimientoOn && IsMaderaOn && IsPesqueraOn
                && IsQuímicaOn && IsSanidadOn && IsSeguridadOn && IsSocioculturalesOn && IsTextilOn
                && IsTransporteOn && IsVidrioOn)
            {
                return resultMaster;
            }

            List<INPAL> resutFiltered = new List<INPAL>();

            if (IsAgrariaOn)
            {
                List<INPAL> result = resultMaster.FindAll(q => q.Activity == Constants.Agraria);
                resutFiltered.AddRange(result);
            }

            if (IsEdificacionOn)
            {
                List<INPAL> result = resultMaster.FindAll(q => q.Activity == Constants.Edificacion);
                resutFiltered.AddRange(result);
            }

            if (IsFabricacionOn)
            {
                List<INPAL> result = resultMaster.FindAll(q => q.Activity == Constants.Fabricacion);
                resutFiltered.AddRange(result);
            }
            if (IsDeportivaOn)
            {
                List<INPAL> result = resultMaster.FindAll(q => q.Activity == Constants.Deportiva);
                resutFiltered.AddRange(result);
            }
            if (IsGestionOn)
            {
                List<INPAL> result = resultMaster.FindAll(q => q.Activity == Constants.Gestion);
                resutFiltered.AddRange(result);
            }
            if (IsGraficasOn)
            {
                List<INPAL> result = resultMaster.FindAll(q => q.Activity == Constants.Graficas);
                resutFiltered.AddRange(result);
            }
            if (IsArtesOn)
            {
                List<INPAL> result = resultMaster.FindAll(q => q.Activity == Constants.Artes);
                resutFiltered.AddRange(result);
            }
            if (IsComercioOn)
            {
                List<INPAL> result = resultMaster.FindAll(q => q.Activity == Constants.Comercio);
                resutFiltered.AddRange(result);
            }
            if (IsElectricidadOn)
            {
                List<INPAL> result = resultMaster.FindAll(q => q.Activity == Constants.Electricidad);
                resutFiltered.AddRange(result);
            }
            if (IsEnergiaOn)
            {
                List<INPAL> result = resultMaster.FindAll(q => q.Activity == Constants.Energia);
                resutFiltered.AddRange(result);
            }
            if (IsHosteleriaOn)
            {
                List<INPAL> result = resultMaster.FindAll(q => q.Activity == Constants.Hosteleria);
                resutFiltered.AddRange(result);
            }
            if (IsPersonalOn)
            {
                List<INPAL> result = resultMaster.FindAll(q => q.Activity == Constants.Personal);
                resutFiltered.AddRange(result);
            }
            if (IsImagenOn)
            {
                List<INPAL> result = resultMaster.FindAll(q => q.Activity == Constants.Imagen);
                resutFiltered.AddRange(result);
            }
            if (IsAlimentariasOn)
            {
                List<INPAL> result = resultMaster.FindAll(q => q.Activity == Constants.Alimentarias);
                resutFiltered.AddRange(result);
            }
            if (IsExtractivasOn)
            {
                List<INPAL> result = resultMaster.FindAll(q => q.Activity == Constants.Extractivas);
                resutFiltered.AddRange(result);
            }
            if (IsInformaticaOn)
            {
                List<INPAL> result = resultMaster.FindAll(q => q.Activity == Constants.Informatica);
                resutFiltered.AddRange(result);
            }
            if (IsMantenimientoOn)
            {
                List<INPAL> result = resultMaster.FindAll(q => q.Activity == Constants.Mantenimiento);
                resutFiltered.AddRange(result);
            }
            if (IsMaderaOn)
            {
                List<INPAL> result = resultMaster.FindAll(q => q.Activity == Constants.Madera);
                resutFiltered.AddRange(result);
            }
            if (IsPesqueraOn)
            {
                List<INPAL> result = resultMaster.FindAll(q => q.Activity == Constants.Pesquera);
                resutFiltered.AddRange(result);
            }
            if (IsQuímicaOn)
            {
                List<INPAL> result = resultMaster.FindAll(q => q.Activity == Constants.Química);
                resutFiltered.AddRange(result);
            }
            if (IsSanidadOn)
            {
                List<INPAL> result = resultMaster.FindAll(q => q.Activity == Constants.Sanidad);
                resutFiltered.AddRange(result);
            }
            if (IsSeguridadOn)
            {
                List<INPAL> result = resultMaster.FindAll(q => q.Activity == Constants.Seguridad);
                resutFiltered.AddRange(result);
            }
            if (IsSocioculturalesOn)
            {
                List<INPAL> result = resultMaster.FindAll(q => q.Activity == Constants.Socioculturales);
                resutFiltered.AddRange(result);
            }
            if (IsTextilOn)
            {
                List<INPAL> result = resultMaster.FindAll(q => q.Activity == Constants.Textil);
                resutFiltered.AddRange(result);
            }
            if (IsTransporteOn)
            {
                List<INPAL> result = resultMaster.FindAll(q => q.Activity == Constants.Transporte);
                resutFiltered.AddRange(result);
            }
            if (IsVidrioOn)
            {
                List<INPAL> result = resultMaster.FindAll(q => q.Activity == Constants.Vidrio);
                resutFiltered.AddRange(result);
            }

            return resutFiltered;
        }

        public async Task<List<INPAL>> GetINPALListByPolygonAsync(string polygon)
        {
            List<INPAL> result = await _database.Table<INPAL>()
                                        .Where(x => x.Polygon == polygon)
                                        .OrderBy(x => x.Name)
                                        .ToListAsync()
                                        .ConfigureAwait(false);
            return result;
        }

        public async Task<List<INPAL>> GetINPALFilteredAsync(List<INPAL> resultMaster, string polygon)
        {
            List<INPAL> result = await GetINPALListByPolygonAsync(polygon);
            foreach (INPAL item in result)
            {
                resultMaster.Add(item);
            }

            return resultMaster;
        }

        public async Task<List<INPAL>> GetINPALAsync()
        {
            if (!IsInitialized)
            {
                await Init();
            }

            List<INPAL> companies = await _database.Table<INPAL>()
                                           .ToListAsync()
                                           .ConfigureAwait(false);
            return companies;
        }

        public async Task<List<INPAL>>GetActivitiesAsync()
        {
            if (!IsInitialized)
            {
                await Init();
            }

            string SQLstring = string.Format("select distinct Activity from INPAL order by Activity asc");
            List<INPAL> result = await _database.QueryAsync<INPAL>(SQLstring); 
            return result;
        }

        public async Task<bool> DeleteSQLiteyAsync()
        {
            try
            {
                _database = DependencyService.Get<ISQLite>().GetAsyncConnection();

                await _database.DeleteAllAsync<INPAL>();
                return true;                
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"ERROR {@}", ex.Message);
                MessagingCenter.Send<DataManager, Exception>(this, "ExceptionOccurred", ex);
                return false;
            }
        }
        #endregion

        #region Fecha
        public async Task<List<Fecha>> GetFechaAsync()
        {
            if (!IsInitialized)
            {
                await Init();
            }

            List<Fecha> fecha = await _database.Table<Fecha>()
                                                    .ToListAsync()
                                                    .ConfigureAwait(false);
            return fecha;
        }

        public async Task<bool> SaveFechaAsync(Fecha item)
        {
            try
            {
                //Si no existe la BD-SQLite, la creo
                if (!IsInitialized)
                {
                    await Init();
                }

                await _database.InsertAsync(item).ConfigureAwait(false);

                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"ERROR {@}", ex.Message);
                MessagingCenter.Send<DataManager, Exception>(this, "ExceptionOccurred", ex);
                return false;
            }
        }

        public async Task<bool> DeleteFechaAsync()
        {
            try
            {
                _database = DependencyService.Get<ISQLite>().GetAsyncConnection();

                await _database.DeleteAllAsync<Fecha>();
                return true;
            }

            catch (Exception ex)
            {
                Debug.WriteLine(@"ERROR {@}", ex.Message);
                MessagingCenter.Send<DataManager, Exception>(this, "ExceptionOccurred", ex);
                return false;

            }
        }
        #endregion
    }
}