﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace INPAL
{
    public class RestManager
    {
        #region Properties

        public RestManager()
        {
            IRestServices = DependencyService.Get<IRestServices>();
        }

        public RestManager(IRestServices iRestServices)
        {
            IRestServices = iRestServices;
        }

        static RestManager _instance;
        public static RestManager Instance
        {
            get
            {
                return _instance ?? (_instance = new RestManager());
            }
        }

        private IRestServices IRestServices;

        #endregion

        #region Server
        
        public async Task<List<INPAL>> GetINPALList()
        {
            return await IRestServices.GetINPALListAsync();
        }

        public async Task<List<Fecha>> GetFechaList()
        {
            return await IRestServices.GetFechaAsync();
        }
        #endregion
    }
}
